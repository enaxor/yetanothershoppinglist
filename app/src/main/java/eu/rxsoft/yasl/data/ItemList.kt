package eu.rxsoft.yasl.data

import java.util.*

class ItemList(
    var id: Long,
    var name: String,
    var date: Date,
    var items: ArrayList<Item>) {

    override fun toString(): String {
        return "ItemList id=$id, name=$name, date=$date, items=${items.joinToString()}}"
    }

}
