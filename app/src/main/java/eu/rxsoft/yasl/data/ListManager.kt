package eu.rxsoft.yasl.data

import java.sql.SQLException

object ListManager {

    private lateinit var db : DatabaseHelper

    fun init (db:DatabaseHelper) {
        this.db = db
    }

    // Current List
    @Throws(SQLException::class)
    fun setCurrentList(basket: Array<Long>) {
        if (!this::db.isInitialized) {
            throw SQLException()
        }
        db.clearCurrentList()
        db.createItemsList(1, basket)
    }

    @Throws(SQLException::class)
    fun checkItem(item : Item) {
        if (!this::db.isInitialized) {
            throw SQLException()
        }
        db.updateListItem(1, item.id, item.checked)
    }

    @Throws(SQLException::class)
    fun addItem(name : String, category: Long = 1) : Long {
        if (!this::db.isInitialized) {
            throw SQLException()
        }
        // check if item does not already exist
        var id : Long ? = db.getItemIdByName(name)
        if (id == null ) {
            id = db.createItem(name, category)
        } else if (category > 1) { // != than the default category
            db.updateItem(id, name, category)
        }
        return id
    }

    @Throws(SQLException::class)
    fun deleteItem(id: Long) {
        if (!this::db.isInitialized) {
            throw SQLException()
        }
        db.deleteItem(id)
    }

    @Throws(SQLException::class)
    fun getCurrentList(): ItemList {
        if (!this::db.isInitialized) {
            throw SQLException()
        }
        return db.getCurrentList()
    }

    @Throws(SQLException::class)
    fun addItemToCurrentList(idItem: Long) {
        if (!this::db.isInitialized) {
            throw SQLException()
        }
        db.createItemList(1, idItem)
    }

    @Throws(SQLException::class)
    fun deleteItemFromCurrentList(idItem: Long) {
        if (!this::db.isInitialized) {
            throw SQLException()
        }
        db.deleteListItem(1, idItem)
    }

    @Throws(SQLException::class)
    fun clearCurrentList() {
        if (!this::db.isInitialized) {
            throw SQLException()
        }
        db.deleteList(1)
    }

    @Throws(SQLException::class)
    // Archived shopping list
    fun getListArchived() : ArrayList<ItemList>  {
        if (!this::db.isInitialized) {
            throw SQLException()
        }
        return db.getArchivedList()
    }

    @Throws(SQLException::class)
    // set archive back to current
    fun loadArchived(id : Long) {
        if (!this::db.isInitialized) {
            throw SQLException()
        }
        val current : ItemList = db.getCurrentList()
        current.items = db.getItemsForList(id)
        db.updateList(current)
    }

    @Throws(SQLException::class)
    // archive current lists
    fun archive(list: ItemList) {
        if (!this::db.isInitialized) {
            throw SQLException()
        }
        if (list.name == "") {
            list.name = list.date.toString()
        }
        list.id = db.createList()
        db.updateList(list)
    }

    @Throws(SQLException::class)
    fun deleteArchivedList(id : Long) {
        if (!this::db.isInitialized) {
            throw SQLException()
        }
        db.deleteList(id)
    }

    // Items
    @Throws(SQLException::class)
    fun getItems(orderByCategory : Boolean = false): ArrayList<Item> {
        if (!this::db.isInitialized) {
            throw SQLException()
        }
        val ret =  db.getAllItems()
        if (orderByCategory) {
            // overwrite the default category order
            ret.sortBy { item -> item.category.id }
        }
        return ret
    }

    @Throws(SQLException::class)
    fun getCategory(full : Boolean = true) : ArrayList<Category> {
        if (!this::db.isInitialized) {
            throw SQLException()
        }
        val ret = db.getAllCategory()
        if (!full) {
            // remove the non-editable default category
            ret.removeAt(0)
        }
        return ret
    }

    @Throws(SQLException::class)
    fun updateItem(idItem: Long, name: String? , idCategory: Long?) {
        if (!this::db.isInitialized) {
            throw SQLException()
        }
        db.updateItem(itemId = idItem, name = name, category = idCategory)
    }

    @Throws(SQLException::class)
    fun addCategory(name: String) {
        if (!this::db.isInitialized) {
            throw SQLException()
        }
        var exist = false
        for (c in getCategory(true)) {
            if ( c.name == name ) {
                exist = true
                break
            }
        }
        if (!exist) {
            db.createCategory(name)
        }
    }

    @Throws(SQLException::class)
    fun deleteCategory(id: Long) {
        if (!this::db.isInitialized) {
            throw SQLException()
        }
        db.deleteCategory(id)
    }

    @Throws(SQLException::class)
    fun updateCategory(id: Long, name: String?, selectedIcon: Int?, selectedColor: Int?) {
        if (!this::db.isInitialized) {
            throw SQLException()
        }
        db.updateCategory(id, name, selectedIcon, selectedColor)
    }

}
