package eu.rxsoft.yasl.data

class Category (
    var id: Long,
    var name: String,
    var icon: Int,
    var color: Int) {

    override fun toString(): String {
            return "Category with id=$id name=$name icon $icon and color $color"
    }
}