package eu.rxsoft.yasl.data

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteDatabase.CREATE_IF_NECESSARY
import android.database.sqlite.SQLiteOpenHelper
import eu.rxsoft.yasl.R
import eu.rxsoft.yasl.ui.adapter.handler.CategoryResourceHandler
import java.util.*
import kotlin.collections.ArrayList
import kotlin.random.Random

class DatabaseHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {

        private const val DATABASE_VERSION = 4
        private const val DATABASE_NAME = "yasl.db"
        // tables name
        private const val TABLE_ITEM = "item"
        private const val TABLE_CATEGORY = "category"
        private const val TABLE_ITEMLIST = "itemList"
        private const val TABLE_LIST = "list"
        // item columns names (id, name, category)
        private const val ITEM_ID = "id"
        private const val ITEM_NAME = "name"
        private const val ITEM_CATEGORY = "idCategory"
        // category columns names (id, name, icon)
        private const val CATEGORY_ID = "id"
        private const val CATEGORY_NAME = "name"
        private const val CATEGORY_ICON = "icon"
        private const val CATEGORY_COLOR = "color"
        // itemlist columns names
        private const val ITEMLIST_ITEM_ID = "idItem"
        private const val ITEMLIST_LIST_ID = "idList"
        private const val ITEMLIST_CHECKED = "checked"
        // list columns names
        private const val LIST_ID = "id" // id == 1 is current list
        private const val LIST_NAME = "name"
        private const val LIST_DATE = "created"
    }

    override fun onCreate(db: SQLiteDatabase) { // table creation
        val createCategoryTable = ("CREATE TABLE " + TABLE_CATEGORY + " ("
                + CATEGORY_ID + " INTEGER PRIMARY KEY, " + CATEGORY_NAME + " TEXT NOT NULL UNIQUE , " + CATEGORY_ICON + " INTEGER NOT NULL , " + CATEGORY_COLOR + " INTEGER NOT NULL ) ")
        val createItemTable = ("CREATE TABLE " + TABLE_ITEM + " ("
                + ITEM_ID + " INTEGER PRIMARY KEY, " + ITEM_NAME + " TEXT NOT NULL, " + ITEM_CATEGORY + " INTEGER , "
                + " FOREIGN KEY ( " + ITEM_CATEGORY + " ) REFERENCES " + TABLE_CATEGORY + " ( " + CATEGORY_ID + " ) ) ")
        val createListTable = ("CREATE TABLE " + TABLE_LIST + "("
                + LIST_ID + " INTEGER PRIMARY KEY, " + LIST_NAME + " TEXT NOT NULL, " + LIST_DATE + " BIGINTEGER NOT NULL )")
        val createItemListTable = ("CREATE TABLE " + TABLE_ITEMLIST +
                " ( " + ITEMLIST_ITEM_ID + " INTEGER, " + ITEMLIST_LIST_ID + " INTEGER , " + ITEMLIST_CHECKED + " BOOLEAN DEFAULT 0, " +
                " FOREIGN KEY ( " + ITEMLIST_ITEM_ID + " ) REFERENCES " + TABLE_ITEM + " ( " + ITEM_ID + " ) , " +
                " FOREIGN KEY ( " + ITEMLIST_LIST_ID + " ) REFERENCES " + TABLE_LIST + " ( " + LIST_ID + " ) , " +
                " PRIMARY KEY ( " + ITEMLIST_LIST_ID + " , " + ITEMLIST_ITEM_ID + " ) ) ")
        db.execSQL(createCategoryTable)
        db.execSQL(createItemTable)
        db.execSQL(createListTable)
        db.execSQL(createItemListTable)
        insertTestCategory(db) // create category
        insertTestItems(db) // create items
        createCurrentList(db) //create current list
    }

    private fun insertTestCategory(db: SQLiteDatabase) {
        createCategory(db, 1, "", CategoryResourceHandler.iconList.indexOf(R.drawable.openmoji_1f374_black), Random.nextInt(0, CategoryResourceHandler.colorList.size))
        createCategory(db, 2, "groceries", CategoryResourceHandler.iconList.indexOf(R.drawable.openmoji_1f36b_black), Random.nextInt(0, CategoryResourceHandler.colorList.size))
        createCategory(db, 3, "dry food", CategoryResourceHandler.iconList.indexOf(R.drawable.openmoji_1f957_black ), Random.nextInt(0, CategoryResourceHandler.colorList.size))
        createCategory(db, 4, "fresh", CategoryResourceHandler.iconList.indexOf(R.drawable.openmoji_2744_black ), Random.nextInt(0, CategoryResourceHandler.colorList.size))
        createCategory(db, 5, "frozen", CategoryResourceHandler.iconList.indexOf(R.drawable.openmoji_1f9ca_black ), Random.nextInt(0, CategoryResourceHandler.colorList.size))
        createCategory(db, 6, "can", CategoryResourceHandler.iconList.indexOf(R.drawable.openmoji_1f96b_black), Random.nextInt(0, CategoryResourceHandler.colorList.size))
        createCategory(db, 7, "pets", CategoryResourceHandler.iconList.indexOf(R.drawable.openmoji_1f431_black ), Random.nextInt(0, CategoryResourceHandler.colorList.size))
        createCategory(db, 8, "household", CategoryResourceHandler.iconList.indexOf(R.drawable.openmoji_1f9f4_black ), Random.nextInt(0, CategoryResourceHandler.colorList.size))
        createCategory(db, 9, "fruit", CategoryResourceHandler.iconList.indexOf(R.drawable.openmoji_1f34f_black ), Random.nextInt(0, CategoryResourceHandler.colorList.size))
        createCategory(db, 10, "vegetable", CategoryResourceHandler.iconList.indexOf(R.drawable.openmoji_1fad1_black), Random.nextInt(0, CategoryResourceHandler.colorList.size))
        createCategory(db, 11, "occasional", CategoryResourceHandler.iconList.indexOf(R.drawable.openmoji_1f378_black ), Random.nextInt(0, CategoryResourceHandler.colorList.size))
    }

    private fun insertTestItems(db: SQLiteDatabase) {
        createItem(db, "cereals" , 2)
        createItem(db, "nuts" , 2)
        createItem(db, "seeds" , 2)
        createItem(db, "dried fruit" , 2)
        createItem(db, "chocolate" , 2)
        createItem(db, "potato chips" , 2)
        createItem(db, "tea" , 2)
        createItem(db, "coffee" , 2)
        createItem(db, "honey" , 2)
        createItem(db, "vegetable oil" , 2)
        createItem(db, "water" , 2)
        createItem(db, "food complement" , 2)

        createItem(db, "pasta" , 3)
        createItem(db, "quinoa"  , 3)
        createItem(db, "popcorn"  , 3)
        createItem(db, "whole grain rice"  , 3)
        createItem(db, "lentils"  , 3)
        createItem(db, "chickpea"  , 3)
        createItem(db, "soy protein"  , 3)
        createItem(db, "beans"  , 3)
        createItem(db, "flour"  , 3)
        createItem(db, "unrefined sugar"  , 3)

        createItem(db, "butter"  , 4)
        createItem(db, "cheese" , 4)
        createItem(db, "jam" , 4)
        createItem(db, "ketchup" , 4)
        createItem(db, "mustard" , 4)
        createItem(db, "milk" , 4)

        createItem(db, "frozen pea" , 5)
        createItem(db, "frozen red berries" , 5)

        createItem(db, "canned peeled tomatoes" , 6)
        createItem(db, "canned corn" , 6)
        createItem(db, "canned red beans" , 6)

        createItem(db, "banana" , 9)
        createItem(db, "apple" , 9)
        createItem(db, "pear" , 9)
        createItem(db, "orange" , 9)
        createItem(db, "avocado" , 9)
        createItem(db, "tangerines" , 9)
        createItem(db, "kiwi" , 9)
        createItem(db, "lemon" , 9)

        createItem(db, "potatoes" , 10 )
        createItem(db, "leeks" , 10 )
        createItem(db, "mushroom" , 10 )
        createItem(db, "salad" , 10 )
        createItem(db, "carrots" , 10 )

        createItem(db, "cat food" , 7 )

        createItem(db, "toothpaste"  , 8 )
        createItem(db, "soap" , 8 )
        createItem(db, "baking soda" , 8 )

        createItem(db, "soy sauce" , 11 )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS $TABLE_ITEMLIST")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_LIST")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_ITEM")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_CATEGORY")
        // Create tables again
        onCreate(db)
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }

    // category CRUD
    private fun createCategory(db: SQLiteDatabase, id : Long, name: String, icon: Int = 0, color: Int = 0) {
        val values = ContentValues()
        values.put(CATEGORY_ID, id)
        values.put(CATEGORY_NAME, name.capitalize())
        values.put(CATEGORY_ICON, icon)
        values.put(CATEGORY_COLOR, color)
        // Inserting Row
        db.insert(TABLE_CATEGORY, null, values)
    }

    fun createCategory(name : String, icon : Int = 0, color: Int = 0) {
        val db = writableDatabase
        val values = ContentValues()
        values.put(CATEGORY_NAME, name.capitalize())
        values.put(CATEGORY_ICON, icon)
        values.put(CATEGORY_COLOR, color)
        // Inserting Row
        db.insert(TABLE_CATEGORY, null, values)
        db.close()
    }

    fun updateCategory(id : Long, name: String?, icon: Int?, color : Int?) {
        val db = writableDatabase
        val values = ContentValues()
        if (color != null) {
            values.put(CATEGORY_COLOR, color)
        }
        if (icon != null) {
            values.put(CATEGORY_ICON, icon)
        }
        if (name != null) {
            values.put(CATEGORY_NAME, name.capitalize())
        }
        // Inserting Row
        db.update(TABLE_CATEGORY, values,  "$CATEGORY_ID = ?", arrayOf(id.toString()))
        db.close()
    }

    fun deleteCategory(id: Long) {
        val db = writableDatabase
        // orphan items with this category
        val values = ContentValues()
        values.put(ITEM_CATEGORY, 1)
        db.update(
            TABLE_ITEM,
            values,
            "$ITEM_CATEGORY = ?",
            arrayOf(id.toString())
        )
        // delete category
        db.delete(
            TABLE_CATEGORY,
            "$CATEGORY_ID = ?",
            arrayOf(id.toString())
        )
        db.close()
    }

    fun getAllCategory() : ArrayList<Category> {
        val db = readableDatabase
        val ret : ArrayList<Category> = ArrayList()
        val cursor: Cursor? = db.query(
            TABLE_CATEGORY, arrayOf(
                CATEGORY_ID,
                CATEGORY_NAME,
                CATEGORY_ICON,
                CATEGORY_COLOR
            ),  null, null, null, null, CATEGORY_NAME, null
        )
        if (cursor != null && cursor.count > 0) {
            cursor.moveToFirst()
            do {
                Category(cursor.getLong(0), cursor.getString(1), cursor.getInt(2), cursor.getInt(3))
                    .let { ret.add(it) }
            } while (cursor.moveToNext())
            cursor.close()
        }
        db.close()
        return ret
    }

    // Item CRUD
    private fun createItem(db : SQLiteDatabase, itemName : String, category: Long = 1)  {
        val values = ContentValues()
        values.put(ITEM_NAME, itemName.capitalize())
        values.put(ITEM_CATEGORY, category)
        // Inserting Row
        db.insert(TABLE_ITEM, null, values)
    }

    fun createItem(itemName : String, category: Long = 1) : Long {
        val db = writableDatabase
        val values = ContentValues()
        values.put(ITEM_NAME, itemName.capitalize())
        values.put(ITEM_CATEGORY, category)
        // Inserting Row
        val id = db.insert(TABLE_ITEM, null, values)
        db.close()
        return id
    }

    fun updateItem(itemId: Long, name: String?, category: Long?)  {
        val db = writableDatabase
        val values = ContentValues()
        if (name != null) {
            values.put(ITEM_NAME, name.capitalize())
        }
        if (category != null) {
            values.put(ITEM_CATEGORY, category)
        }
        // Inserting Row
        db.update(TABLE_ITEM, values,  "$ITEM_ID = ?", arrayOf(itemId.toString()))
        db.close()
    }

    fun getItemsForList(id : Long) : ArrayList<Item> {
        val db = readableDatabase
        val ret : ArrayList<Item> = ArrayList()
        val query = " SELECT " +
                TABLE_ITEM + "." + ITEM_ID + ", " + TABLE_ITEM + "." + ITEM_NAME + ", " + TABLE_ITEMLIST + "." + ITEMLIST_CHECKED + ", " +
                TABLE_CATEGORY + "." + CATEGORY_ID + ", " + TABLE_CATEGORY + "." + CATEGORY_NAME + ", " + TABLE_CATEGORY + "." + CATEGORY_ICON + ", " + TABLE_CATEGORY + "." + CATEGORY_COLOR +
                " FROM " + TABLE_ITEM +
                " INNER JOIN " + TABLE_CATEGORY + " ON " + TABLE_CATEGORY + "." + CATEGORY_ID + " = " + TABLE_ITEM + "." + ITEM_CATEGORY +
                " INNER JOIN " + TABLE_ITEMLIST + " ON " + TABLE_ITEMLIST + "." + ITEMLIST_ITEM_ID + " = " + TABLE_ITEM + "." + ITEM_ID +
                " WHERE " + TABLE_ITEMLIST + "." + ITEMLIST_LIST_ID + " = ? " +
                " ORDER BY " + TABLE_ITEMLIST + "." + ITEMLIST_CHECKED + ", " + TABLE_CATEGORY + "." + CATEGORY_ID + ", " + TABLE_ITEM + "." + ITEM_NAME
        val cursor: Cursor? =  db.rawQuery(query, arrayOf(id.toString()))
        if (cursor != null) {
            if (cursor.count > 0) {
                cursor.moveToFirst()
                do {
                    val idItem = cursor.getLong(0)
                    val nameItem = cursor.getString(1)
                    val checkedItem = cursor.getInt(2) > 0
                    val categoryItem = Category(cursor.getLong(3), cursor.getString(4), cursor.getInt(5), cursor.getInt(6))
                    // Adding contact to list
                    ret.add(Item(idItem, nameItem, checkedItem, categoryItem))
                } while (cursor.moveToNext())
            }
            cursor.close()
        }
        db.close()
        return ret
    }

    fun getItemIdByName(name : String) : Long ? {
        val db = readableDatabase
        var ret : Long ? = null
        val cursorList: Cursor? = db.query(
            TABLE_ITEM, arrayOf(
                ITEM_ID
            ), "$ITEM_NAME LIKE ? ", arrayOf(name), null, null, null, 1.toString()
        )
        if (cursorList != null && cursorList.count > 0) {
            cursorList.moveToFirst()
            do {
                ret = cursorList.getLong(0)
            } while (cursorList.moveToNext())
            cursorList.close()
        }
        db.close()
        return ret
    }

    fun getAllItems() : ArrayList<Item> {
        val db = readableDatabase
        val ret : ArrayList<Item> = ArrayList()
        val query = " SELECT " +
                TABLE_ITEM + "." + ITEM_ID + ", " + TABLE_ITEM + "." + ITEM_NAME + ", " +
                TABLE_CATEGORY + "." + CATEGORY_ID + ", " + TABLE_CATEGORY + "." + CATEGORY_NAME + ", " + TABLE_CATEGORY + "." + CATEGORY_ICON + ", " + TABLE_CATEGORY + "." + CATEGORY_COLOR +
                " FROM " + TABLE_ITEM +
                " INNER JOIN " + TABLE_CATEGORY + " ON " + TABLE_CATEGORY + "." + CATEGORY_ID + " = " + TABLE_ITEM + "." + ITEM_CATEGORY +
                " ORDER BY " + TABLE_ITEM + "." + ITEM_NAME
        val cursor: Cursor? =  db.rawQuery(query, null)
        if (cursor != null) {
            if (cursor.count > 0) {
                cursor.moveToFirst()
                do {
                    val idItem = cursor.getLong(0)
                    val nameItem = cursor.getString(1)
                    val categoryItem = Category(cursor.getLong(2), cursor.getString(3), cursor.getInt(4), cursor.getInt(5))
                    // Adding contact to list
                    ret.add(Item(idItem, nameItem, false, categoryItem))
                } while (cursor.moveToNext())
            }
            cursor.close()
        }
        db.close()
        return ret
    }

    fun deleteItem(id: Long) {
        val db = writableDatabase
        // delete link with list too
        db.delete(
            TABLE_ITEMLIST,
            "$ITEMLIST_ITEM_ID = ?",
            arrayOf(id.toString())
        )
        db.delete(
            TABLE_ITEM,
            "$ITEM_ID = ?",
            arrayOf(id.toString())
        )
        db.close()
    }

    // ItemList CRUD
    fun createItemList(idList: Long, idItem: Long) {
        val db = writableDatabase
        val values = ContentValues()
        values.put(ITEMLIST_LIST_ID, idList)
        values.put(ITEMLIST_ITEM_ID, idItem)
        // Inserting Row
        db.replace(TABLE_ITEMLIST, null, values)
        db.close()
    }

    fun createItemsList (idList: Long, items : Array<Long>) {
        for (id in items) {
            createItemList(idList, id)
        }
    }

    fun updateListItem (idList: Long, idItem: Long, checked: Boolean) {
        val db = writableDatabase
        // update list
        val values = ContentValues()
        values.put(ITEMLIST_CHECKED, checked)
        db.update(
            TABLE_ITEMLIST,
            values,
            "$ITEMLIST_LIST_ID = ? AND $ITEMLIST_ITEM_ID = ?",
            arrayOf(idList.toString(), idItem.toString())
        )
        db.close()
    }

    fun deleteListItem (idList: Long, idItem: Long) {
        val db = writableDatabase
        db.delete(
            TABLE_ITEMLIST,
            "$ITEMLIST_LIST_ID = ? AND $ITEMLIST_ITEM_ID = ?",
            arrayOf(idList.toString(), idItem.toString())
        )
        db.close()
    }

    fun clearCurrentList () {
        val db = writableDatabase
        db.delete(
            TABLE_ITEMLIST,"$ITEMLIST_LIST_ID = 1", null
        )
        db.close()
    }

    // List CRUD
    private fun createCurrentList(db : SQLiteDatabase) : Long {
        val values = ContentValues()
        values.put(LIST_ID, 1)
        values.put(LIST_DATE, System.currentTimeMillis())
        values.put(LIST_NAME, "")
        // Inserting Row
        return db.insert(TABLE_LIST, null, values)
    }

    fun createList() : Long {
        val db = writableDatabase
        val values = ContentValues()
        values.put(LIST_DATE, System.currentTimeMillis())
        values.put(LIST_NAME, "")
        // Inserting Row
        val id : Long = db.insert(TABLE_LIST, null, values)
        db.close()
        return id
    }

    fun getArchivedList() : ArrayList<ItemList> {
        val db = readableDatabase
        val cursorList: Cursor? = db.query(
            TABLE_LIST, arrayOf(
                LIST_ID
                ), "$LIST_ID !=?", arrayOf(1.toString()), null, null, null, null
        )
        val idsArchivedList : ArrayList<ItemList> = ArrayList()
        if (cursorList != null && cursorList.count > 0) {
            cursorList.moveToFirst()
            do {
                readList(cursorList.getLong(0))?.let { idsArchivedList.add(it) }
            } while (cursorList.moveToNext())
            cursorList.close()
        }
        db.close()
        return idsArchivedList
    }
    fun getCurrentList() : ItemList {
        return readList(1)!!
    }

    private fun readList(id: Long) : ItemList? {
        val db = readableDatabase
        val cursorList: Cursor? = db.query(
            TABLE_LIST, arrayOf(
                LIST_ID,
                LIST_NAME,
                LIST_DATE
            ), "$LIST_ID=?", arrayOf(id.toString()), null, null, null, 1.toString()
        )
        var ret: ItemList? = null
        if (cursorList != null) {
            if (cursorList.count > 0) {
                cursorList.moveToFirst()
                ret = ItemList(
                    cursorList.getLong(0),
                    cursorList.getString(1),
                    Date(cursorList.getLong(2)),
                    ArrayList()
                )
                ret.items = getItemsForList(ret.id)
            }
            cursorList.close()
        }
        db.close()
        return ret
    }


    fun updateList(list: ItemList) {
        // check if list exist
        if (readList(list.id) == null) {
            return
        }
        // update list
        val db = writableDatabase
        val values = ContentValues()
        values.put(LIST_NAME, list.name)
        values.put(LIST_DATE, list.date.time)
        db.update(
            TABLE_LIST,
            values,
            "$LIST_ID = ?",
            arrayOf(list.id.toString())
        )
        // update items
        values.put(LIST_ID, list.id)
        // delete NOT IN
        val sb : StringBuilder = java.lang.StringBuilder()
        sb.append(" (")
        var comma = true
        for (item in list.items) {
            if (comma) {
                sb.append(item.id)
                comma = false
            } else {
                sb.append(",",item.id)
            }
        }
        sb.append(") ")
        // delete not in
        db.delete(
            TABLE_ITEMLIST,
            "$ITEMLIST_LIST_ID = ? AND $ITEMLIST_ITEM_ID NOT IN $sb ",
            arrayOf(list.id.toString())
        )
        db.close()
        // insert new
        createItemsList(list.id, list.items.map { item -> item.id }.toTypedArray())
    }

    fun deleteList(idList: Long) {
        val db = writableDatabase
        db.delete(
            TABLE_ITEMLIST,
            "$ITEMLIST_LIST_ID = ? ",
            arrayOf(idList.toString())
        )
        if (idList != 1.toLong()) {
            db.delete(
                TABLE_LIST,
                "$LIST_ID = ? ",
                arrayOf(idList.toString())
            )
        }
        db.close()
    }

}

//        var bread: Item =
//            Item("bread" )
//        var water: Item =
//            Item("water" )
//        var apple: Item =
//            Item("apple" )
//        var rice: Item =
//            Item("rice" )
//        return arrayListOf(bread,water,apple,rice)
//        var apple: Item =
//            Item("apple" )
//        var pear: Item =
//            Item("pear" )
//        var tea: Item =
//            Item("tea" )
//        var water: Item =
//            Item("water" )
//        var coffee: Item =
//            Item("coffee" )
//        var coing: Item =
//            Item("coing" )
//        var honey: Item =
//            Item("honey" )
//        var rice: Item =
//            Item("rice" )
//        var peanut: Item =
//            Item("peanut" )
//        val formatter1 = SimpleDateFormat("yyyy-MM-dd")
//        var list1 : ItemList = ItemList(0, arrayListOf(pear,coing,apple), formatter1.parse("2018-12-12"))
//        var list2 : ItemList = ItemList(1, arrayListOf(tea,coffee,water), formatter1.parse("2019-12-12"))
//        var list3 : ItemList = ItemList(2, arrayListOf(honey,water,peanut,rice), formatter1.parse("2020-12-12"))
//        return arrayListOf(list1,list2,list3)

// category CRUD
// Adding new category
//    fun addCategory(category: Category) {
//        val db: SQLiteDatabase = this.getWritableDatabase()
//        val values = ContentValues()
//        values.put(CATEGORY_KEY_NAME, category.get_name())
//        values.put(CATEGORY_KEY_MIN, category.get_min())
//        values.put(CATEGORY_KEY_MAX, category.get_max())
//        // Inserting Row
//        db.insert(TABLE_CATEGORY, null, values)
//        db.close() // Closing database connection
//    }
//
//    // Getting single category
//    fun getCategory(id: Int): Category? {
//        val db: SQLiteDatabase = this.getReadableDatabase()
//        val cursor: Cursor? = db.query(
//            TABLE_CATEGORY, arrayOf(
//                CATEGORY_KEY_ID,
//                CATEGORY_KEY_NAME, CATEGORY_KEY_MIN, CATEGORY_KEY_MAX
//            ), "$CATEGORY_KEY_ID=?", arrayOf(id.toString()), null, null, null, null
//        )
//        if (cursor != null) {
//            cursor.moveToFirst()
//            return Category(
//                cursor.getString(0),
//                cursor.getString(1).toInt(),
//                cursor.getString(2).toInt()
//            )
//        }
//        return null
//    }
//
//    // Getting All category
//    fun getAllCategory(): List<Category>? {
//        val categoryList: MutableList<Category> = ArrayList<Category>()
//        val selectQuery = "SELECT * FROM $TABLE_CATEGORY"
//        val db: SQLiteDatabase = this.getReadableDatabase()
//        val cursor: Cursor = db.rawQuery(selectQuery, null)
//        if (cursor.moveToFirst()) {
//            do {
//                val category = Category(
//                    cursor.getString(0).toInt(),
//                    cursor.getString(1),
//                    cursor.getString(2).toInt(),
//                    cursor.getString(3).toInt()
//                )
//                // Adding contact to list
//                categoryList.add(category)
//            } while (cursor.moveToNext())
//        }
//        // return contact list
//        return categoryList
//    }
//
//    // Getting category Count
//    fun getCategoryCount(): Int {
//        val countQuery = "SELECT * FROM $TABLE_CATEGORY"
//        val db: SQLiteDatabase = this.getReadableDatabase()
//        val cursor: Cursor = db.rawQuery(countQuery, null)
//        val count: Int = cursor.getCount()
//        cursor.close()
//        db.close()
//        return count
//    }
//
//    // Updating single category
//    fun updateCategory(category: Category): Int {
//        val db: SQLiteDatabase = this.getWritableDatabase()
//        val values = ContentValues()
//        values.put(CATEGORY_KEY_NAME, category.get_name())
//        values.put(CATEGORY_KEY_MIN, category.get_min())
//        values.put(CATEGORY_KEY_MAX, category.get_max())
//        // updating row
//        return db.update(
//            TABLE_CATEGORY,
//            values,
//            "$CATEGORY_KEY_ID = ?",
//            arrayOf(String.valueOf(category.get_id()))
//        )
//    }
//
//    // Deleting single category
//    fun deleteCategory(category: Category) {
//        val db: SQLiteDatabase = this.getWritableDatabase()
//        db.delete(
//            TABLE_CATEGORY,
//            "$CATEGORY_KEY_ID = ?",
//            arrayOf(String.valueOf(category.get_id()))
//        )
//        db.close()
//    }
//
//    // ingredients CRUD
//// Adding new ingredient
//    fun addIngredient(ingredient: Ingredient) {
//        val db: SQLiteDatabase = this.getWritableDatabase()
//        val values = ContentValues()
//        values.put(INGREDIENT_KEY_NAME, ingredient.get_name()) // ingr Name
//        values.put(INGREDIENT_KEY_CATEGORY, ingredient.get_categoryId()) // ingre categ
//        // Inserting Row
//        db.insert(TABLE_INGREDIENT, null, values)
//        db.close() // Closing database connection
//    }
//
//    // Getting single ingredient
//    fun getIngredient(id: Int): Ingredient? {
//        val db: SQLiteDatabase = this.getReadableDatabase()
//        val cursor: Cursor? = db.query(
//            TABLE_INGREDIENT,
//            arrayOf(
//                INGREDIENT_KEY_ID,
//                INGREDIENT_KEY_NAME, INGREDIENT_KEY_CATEGORY
//            ),
//            "$INGREDIENT_KEY_ID=?",
//            arrayOf(id.toString()),
//            null,
//            null,
//            null,
//            null
//        )
//        if (cursor != null) {
//            cursor.moveToFirst()
//            // return ingredient
//            return Ingredient(
//                cursor.getString(0).toInt(),
//                cursor.getString(1), cursor.getString(2).toInt()
//            )
//        }
//        return null
//    }
//
//    // Getting single ingredient by category
//    fun getIngredientByCategory(category: Int): List<Ingredient>? {
//        val ingredientList: MutableList<Ingredient> = ArrayList<Ingredient>()
//        val db: SQLiteDatabase = this.getReadableDatabase()
//        val cursor: Cursor = db.query(
//            TABLE_INGREDIENT,
//            arrayOf(
//                INGREDIENT_KEY_ID,
//                INGREDIENT_KEY_NAME, INGREDIENT_KEY_CATEGORY
//            ),
//            "$INGREDIENT_KEY_CATEGORY=?",
//            arrayOf(category.toString()),
//            null,
//            null,
//            null,
//            null
//        )
//        if (cursor.moveToFirst()) {
//            do {
//                val ingredient = Ingredient(
//                    cursor.getString(0).toInt(),
//                    cursor.getString(1),
//                    cursor.getString(2).toInt()
//                )
//                ingredientList.add(ingredient)
//            } while (cursor.moveToNext())
//        }
//        // return contact list
//        return ingredientList
//    }
//
//    // Getting all ingredient
//    fun getAllIngredient(): List<Ingredient>? {
//        val ingredientList: MutableList<Ingredient> = ArrayList<Ingredient>()
//        // Select All Query
//        val selectQuery = "SELECT * FROM $TABLE_INGREDIENT"
//        val db: SQLiteDatabase = this.getReadableDatabase()
//        val cursor: Cursor = db.rawQuery(selectQuery, null)
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                val ingredient = Ingredient(
//                    cursor.getString(0).toInt(),
//                    cursor.getString(1),
//                    cursor.getString(2).toInt()
//                )
//                ingredientList.add(ingredient)
//            } while (cursor.moveToNext())
//        }
//        // return contact list
//        return ingredientList
//    }
//
//    // Getting ingr Count
//    fun getIngedientsCount(): Int {
//        val countQuery = "SELECT * FROM $TABLE_INGREDIENT"
//        val db: SQLiteDatabase = this.getReadableDatabase()
//        val cursor: Cursor = db.rawQuery(countQuery, null)
//        val count: Int = cursor.getCount()
//        cursor.close()
//        db.close()
//        // return count
//        return count
//    }
//
//    // Updating single ingredient
//    fun updateIngedient(ingredient: Ingredient): Int {
//        val db: SQLiteDatabase = this.getWritableDatabase()
//        val values = ContentValues()
//        values.put(INGREDIENT_KEY_NAME, ingredient.get_name())
//        values.put(INGREDIENT_KEY_CATEGORY, ingredient.get_categoryId())
//        // updating row
//        return db.update(
//            TABLE_INGREDIENT,
//            values,
//            "$INGREDIENT_KEY_ID = ?",
//            arrayOf(String.valueOf(ingredient.get_id()))
//        )
//    }
//
//    // Deleting single contact
//    fun deleteIngredient(ingredient: Ingredient) {
//        val db: SQLiteDatabase = this.getWritableDatabase()
//        db.delete(
//            TABLE_INGREDIENT,
//            "$INGREDIENT_KEY_ID = ?",
//            arrayOf(String.valueOf(ingredient.get_id()))
//        )
//        db.close()
//    }



//    class Deferrable {
//        private val actions: MutableList<() -> Unit> = mutableListOf()
//
//        fun defer(f: () -> Unit) {
//            actions.add(f)
//        }
//
//        fun execute() {
//            actions.forEach { it() }
//        }
//    }
//
//    fun <T> defer(f: (Deferrable) -> T): T {
//        val deferrable = Deferrable()
//        try {
//            return f(deferrable)
//        } finally {
//            deferrable.execute()
//        }
//    }

// fun getLists() : ArrayList<ItemList> {
//        val db = readableDatabase
//        val cursorList: Cursor? = db.query(
//            TABLE_LIST, arrayOf(
//                LIST_ID
//            ), null, null, null, null, null
//        )
//        val idsArchivedList : ArrayList<ItemList> = ArrayList<ItemList>()
//        if (cursorList != null && cursorList.count > 0) {
//            cursorList.moveToFirst()
//            do {
//                readList(cursorList.getLong(0))?.let { idsArchivedList.add(it) }
//            } while (cursorList.moveToNext())
//            cursorList.close()
//        }
//        db.close()
//        return idsArchivedList
//    }