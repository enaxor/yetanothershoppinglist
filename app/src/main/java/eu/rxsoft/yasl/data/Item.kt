package eu.rxsoft.yasl.data

class Item(
    var id: Long,
    var name: String,
    var checked: Boolean = false,
    var category: Category) {
    override fun toString(): String {
        return "Item with id=$id name=$name checked $checked category $category"
    }
}