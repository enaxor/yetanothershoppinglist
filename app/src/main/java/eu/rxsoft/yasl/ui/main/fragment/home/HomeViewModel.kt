package eu.rxsoft.yasl.ui.main.fragment.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import eu.rxsoft.yasl.data.Item
import eu.rxsoft.yasl.data.ListManager

class HomeViewModel : ViewModel() {
    private val _shoppingList = MutableLiveData<ArrayList<Item>>().apply {
        value = ListManager.getCurrentList().items
    }
    val shoppingList: MutableLiveData<ArrayList<Item>> = _shoppingList
}