package eu.rxsoft.yasl.ui.main.fragment.items

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import eu.rxsoft.yasl.data.Item
import eu.rxsoft.yasl.data.ListManager

class EditItemsViewModel : ViewModel() {

    private val _itemList = MutableLiveData<ArrayList<Item>>().apply {
        value = ListManager.getItems()
    }
    val itemList: LiveData<ArrayList<Item>> = _itemList

}