package eu.rxsoft.yasl.ui.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.os.Build
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.SpinnerAdapter
import eu.rxsoft.yasl.R
import eu.rxsoft.yasl.ui.adapter.handler.CategoryResourceHandler
import kotlinx.android.synthetic.main.spinner_icon_layout.view.*

class SpinnerCategoryAdapter(private var context: Context, private val modeColor : Boolean, private var resList: ArrayList<Int>) :  BaseAdapter(), SpinnerAdapter {

    override fun getCount(): Int {
        return resList.size
    }

    override fun getItem(position: Int): Any {
        return resList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val ret : View = convertView ?: View.inflate(context, R.layout.spinner_icon_layout, null)
        val icon = ret.spinner_icon
        if (modeColor) {
            icon.setBackgroundColor(CategoryResourceHandler.getColor(context, position))
        } else {
            icon.background = CategoryResourceHandler.getDrawable(context, position)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                icon.backgroundTintList = ColorStateList.valueOf(context.resources.getColor(R.color.colorAccent))
            }
        }
        return ret
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val ret : View = convertView ?: View.inflate(context, R.layout.spinner_icon_layout, null)
        val icon = ret.spinner_icon
        if (modeColor) {
            icon.setBackgroundColor(CategoryResourceHandler.getColor(context, position))
        } else {
            icon.background = CategoryResourceHandler.getDrawable(context, position)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                icon.backgroundTintList = ColorStateList.valueOf(context.resources.getColor(R.color.colorAccent))
            }
        }
        return ret
    }

}