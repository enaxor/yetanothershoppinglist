package eu.rxsoft.yasl.ui.main.fragment.about

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import eu.rxsoft.yasl.R
import kotlinx.android.synthetic.main.fragment_about.*


class AboutFragment : Fragment() {

    private lateinit var viewModel: AboutViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel =
            ViewModelProviders.of(this).get(AboutViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_about, container, false)
        viewModel.text.observe(viewLifecycleOwner, Observer {
            html_about.webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                    val i = Intent(Intent.ACTION_VIEW, Uri.parse(url.toString()))
                    context!!.startActivity(i)
                    return true
                }

                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
                    val i = Intent(Intent.ACTION_VIEW, Uri.parse(request.url.toString()))
                    context!!.startActivity(i)
                    return true
                }
            }
            html_about.loadUrl("file:///android_asset/about.html")
        })
        return root
    }
}