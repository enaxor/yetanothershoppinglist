package eu.rxsoft.yasl.ui.main.fragment.category

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import eu.rxsoft.yasl.data.Category
import eu.rxsoft.yasl.data.ListManager

class CategoryViewModel : ViewModel() {

    private val _categoryList = MutableLiveData< ArrayList<Category>>().apply {
        value = ListManager.getCategory(false)
    }
    val categoryList: LiveData<ArrayList<Category>> = _categoryList
}