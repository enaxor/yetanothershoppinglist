package eu.rxsoft.yasl.ui.main

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Build
import android.os.Bundle
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.material.snackbar.Snackbar
import eu.rxsoft.yasl.R
import eu.rxsoft.yasl.data.Item
import eu.rxsoft.yasl.data.ListManager
import eu.rxsoft.yasl.ui.adapter.QuizPagerAdapter
import kotlinx.android.synthetic.main.activity_quiz.*
import kotlinx.android.synthetic.main.dynamic_view_pager.*


class QuizActivity : AppCompatActivity() {

    private val basket: ArrayList<Long> = ArrayList()
    val items: ArrayList<Item> = ListManager.getItems(orderByCategory = true)
    var currentItemIndex: Int = 0
    private var merge: Boolean = false

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, QuizActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)
        // fill extras
        merge = intent.getBooleanExtra("merge", false)
        // init list
        if (items.isEmpty()) {
            // TOASTING FLEE
            Toast.makeText(applicationContext, "Add grocery item first", Toast.LENGTH_SHORT).show()
            this.finish()
        }
        // create view pager
        val pagerAdapter = QuizPagerAdapter(this, items)
        pager.adapter = pagerAdapter
        pager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                currentItemIndex = position
                // update buttons
                updateUI()
            }
        })
        // load first item page
        updateUI()
        // buttons
        quiz_buttonYES.setOnClickListener {
            // color it
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                it.backgroundTintList =
                    ColorStateList.valueOf(resources.getColor(R.color.greenAccent))
            }
            // add to basket
            processShopAction(true)
            // next page
            getNextItemIndex()
            pager.setCurrentItem(currentItemIndex, true)
        }
        quiz_buttonNO.setOnLongClickListener {
            val item = items[currentItemIndex]
            val builderX = AlertDialog.Builder(it.context)
            builderX.setTitle("Delete item from all lists ?")
            builderX.setMessage("${item.name} will be deleted from database and all lists")
            builderX.setPositiveButton("Delete") { dialog, _ ->
                // remove from basket if exist
                processShopAction(false)
                // delete view from pager
                pager.removeViewAt(currentItemIndex)
                // delete item from pager adapter
                (pager.adapter as QuizPagerAdapter).removeItem(currentItemIndex)
                // delete item from db
                ListManager.deleteItem(item.id)
                // close dialog
                dialog.dismiss()
                // next page
                pager.setCurrentItem(currentItemIndex, true)
                updateUI()
            }
            builderX.setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
            builderX.show()
            true
        }
        quiz_buttonNO.setOnClickListener {
            // remove from basket
            processShopAction(false)
            // next page
            getNextItemIndex()
            pager.setCurrentItem(currentItemIndex, true)
        }
        quiz_buttonSkipCategory.setOnClickListener {
            // next page with next category
            getNextItemIndex(true)
            pager.setCurrentItem(currentItemIndex, true)
        }
        quiz_buttonDone.setOnClickListener {
            leaveSavingList()
        }
    }

    override fun onBackPressed() {
        if (basket.isNotEmpty()) {
            val builderX = AlertDialog.Builder(this)
            builderX.setTitle("Save generated list ?")
            builderX.setMessage("${basket.size} items in basket")
            builderX.setPositiveButton(R.string.yes) { dialog, _ ->
                leaveSavingList()
                dialog.dismiss()
                super.onBackPressed()
            }
            builderX.setNegativeButton(R.string.no) { dialog, _ ->
                dialog.cancel()
                super.onBackPressed()
            }
            builderX.show()
        } else {
            super.onBackPressed()
        }
    }

    private fun updateUI() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (basket.contains(items[currentItemIndex].id)) {
                quiz_buttonYES.backgroundTintList =
                    ColorStateList.valueOf(resources.getColor(R.color.greenAccent))
                (quiz_buttonYES as ImageButton).setImageResource(R.drawable.openmoji_e0a3_color)
            } else {
                quiz_buttonYES.backgroundTintList =
                    ColorStateList.valueOf(resources.getColor(android.R.color.transparent))
                (quiz_buttonYES as ImageButton).setImageResource(R.drawable.openmoji_1f197_white)
            }
        }
    }

    private fun getNextItemIndex(skippingCurrentCategory: Boolean = false) {
        if (items.getOrNull(currentItemIndex) != null) {
            if (skippingCurrentCategory) {
                // increment of item list until != category found
                val currentCategoryId = items[currentItemIndex].category.id
                while (currentItemIndex + 1 < items.size) {
                    if (items[currentItemIndex].category.id != currentCategoryId) {
                        return
                    }
                    currentItemIndex += 1
                }
            } else if (currentItemIndex + 1 < items.size) {
                currentItemIndex += 1
                return
            }
        }
        leaveSavingList()
    }

    private fun leaveSavingList() {
        // save data
        if (basket.isNotEmpty()) {
            val newList = basket.toTypedArray().toMutableList()
            // merge data
            if (merge) {
                val currentList = ListManager.getCurrentList()
                for (item in currentList.items) {
                    if (!newList.contains(item.id)) {
                        newList.add(item.id)
                    }
                }
            }
            ListManager.setCurrentList(newList.toTypedArray())
        }
        // close activity
        this.finish()
    }

    private fun processShopAction(buy: Boolean) {
        // add current food
        val currentFood = items[currentItemIndex]
        val inBasket = basket.contains(currentFood.id)
        if (buy && !inBasket) {
            basket.add(currentFood.id)
            Snackbar.make(
                quiz_layout_anchor,
                "Added ${currentFood.name} to basket !",
                Snackbar.LENGTH_SHORT
            ).setAction(R.string.dismiss) {}.show()
        } else if (!buy && inBasket) {
            // remove item from basket
            basket.remove(currentFood.id)
            Snackbar.make(
                quiz_layout_anchor,
                "Removed ${currentFood.name} from basket.",
                Snackbar.LENGTH_SHORT
            ).setAction(R.string.dismiss) {}.show()
        }
    }

}
