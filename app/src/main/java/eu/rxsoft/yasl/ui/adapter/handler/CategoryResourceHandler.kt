package eu.rxsoft.yasl.ui.adapter.handler

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import eu.rxsoft.yasl.R
import eu.rxsoft.yasl.data.Category

object CategoryResourceHandler {
    val iconList: ArrayList<Int> = arrayListOf(
        R.drawable.openmoji_1f36b_black,
        R.drawable.openmoji_1f378_black,
        R.drawable.openmoji_1f197_white,
        R.drawable.openmoji_1f9f8_black,
        R.drawable.openmoji_1f520_white,
        R.drawable.openmoji_1f33d_black,
        R.drawable.openmoji_e25f_white,
        R.drawable.openmoji_2139_white,
        R.drawable.openmoji_1f4c5_white,
        R.drawable.openmoji_1f35e_black,
        R.drawable.openmoji_1f330_black,
        R.drawable.openmoji_1f9ca_black,
        // R.drawable.openmoji_e0a4_color,
        R.drawable.openmoji_e261_black,
        R.drawable.openmoji_1f9f9_black,
        R.drawable.openmoji_1f9f4_black,
        R.drawable.openmoji_e262_white,
        R.drawable.openmoji_e25f_black,
        R.drawable.openmoji_1f967_black,
        R.drawable.openmoji_1f34f_black,
        R.drawable.openmoji_e2d2_black,
     //   R.drawable.openmoji_e0a3_color,
        R.drawable.openmoji_1f96b_black,
        R.drawable.openmoji_1f957_black,
        R.drawable.openmoji_1fa9f_white,
        R.drawable.openmoji_1f431_black,
        R.drawable.openmoji_1fad1_black,
        R.drawable.openmoji_1f374_black,
        R.drawable.openmoji_2744_black,
        R.drawable.openmoji_1f35d_black,
        R.drawable.openmoji_2615_black,
        R.drawable.openmoji_1f344_black,
        R.drawable.openmoji_1f962_black
    )

    val colorList: ArrayList<Int> = arrayListOf(
        android.R.color.holo_blue_bright,
        android.R.color.holo_blue_dark,
        android.R.color.holo_green_dark,
        android.R.color.holo_green_light,
        android.R.color.holo_orange_dark,
        android.R.color.holo_orange_light,
        android.R.color.holo_purple,
        android.R.color.holo_red_light
    )

    fun getColor(context : Context, position : Int) : Int {
        return context.resources.getColor(colorList[position])
    }

    fun getDrawable(context : Context, position : Int) : Drawable {
        return context.resources.getDrawable(iconList[position])
    }

    fun getCategoryDrawable(context: Context, category : Category) : Drawable {
        val ret =
            getDrawable(
                context,
                category.icon
            )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ret.setTint(
                getColor(
                    context,
                    category.color
                )
            )
        }
        return ret
    }

}