package eu.rxsoft.yasl.ui.main.fragment.backup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class BackupViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is the import/export Fragment"
    }
    val text: LiveData<String> = _text

}