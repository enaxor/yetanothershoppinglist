package eu.rxsoft.yasl.ui.main.fragment.items

import android.app.AlertDialog
import android.os.Bundle
import android.text.InputType
import android.view.*
import android.widget.EditText
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import eu.rxsoft.yasl.R
import eu.rxsoft.yasl.data.Item
import eu.rxsoft.yasl.data.ListManager
import eu.rxsoft.yasl.ui.adapter.ItemEditionListAdapter
import eu.rxsoft.yasl.ui.adapter.handler.OrderType
import kotlinx.android.synthetic.main.activity_item_list_edit.*

class EditItemsFragment : Fragment() {

    private lateinit var editItemsViewModel: EditItemsViewModel
    private lateinit var adapter : ItemEditionListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        editItemsViewModel =
            ViewModelProviders.of(this).get(EditItemsViewModel::class.java)
        val root = inflater.inflate(R.layout.activity_item_list_edit, container, false)
        // menu
        setHasOptionsMenu(true)
        // get list from view model
        editItemsViewModel.itemList.observe(viewLifecycleOwner, Observer {
            // create adapter
            adapter = ItemEditionListAdapter(it)
            item_edition_listView.adapter = adapter
            // fix recycler view layout
            item_edition_listView.layoutManager = LinearLayoutManager(context)
        })
        return root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_edit_item, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_sort_item -> {
            Runnable {
                when (adapter.reorderList()) {
                    OrderType.Created -> item.setIcon(R.drawable.openmoji_1f4c5_white)
                    OrderType.Alphanumeric -> item.setIcon(R.drawable.openmoji_1f520_white)
                    OrderType.Category -> item.setIcon(R.drawable.openmoji_1fa9f_white)
                }
                adapter.notifyDataSetChanged()

            }.run()
            true
        }
        R.id.action_add_item -> {
            // input
            val input = EditText(context!!)
            input.inputType = InputType.TYPE_CLASS_TEXT
            input.hint = "ex: apple"
            input.setSingleLine()
            // container
            val container = FrameLayout(context!!)
            val params = FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            params.leftMargin = resources.getDimensionPixelSize(R.dimen.dialog_margin)
            input.layoutParams = params
            container.addView(input)
            // builder
            val builderX = AlertDialog.Builder(context!!)
            builderX.setTitle("Create new grocery item")
            builderX.setView(container)
            builderX.setPositiveButton(R.string.ok) { dialog, _ ->
                val inputItemName : String = input.text.toString().trim()
                if (inputItemName.isNotEmpty()) {
                    ListManager.addItem(inputItemName)
                    Runnable {
                        adapter.setList(ListManager.getItems())
                        adapter.notifyDataSetChanged()
                    }.run()
                }
                dialog.dismiss()
            }
            builderX.setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
            builderX.show()
            true
        }
        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

}
