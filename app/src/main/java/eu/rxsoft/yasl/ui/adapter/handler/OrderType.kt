package eu.rxsoft.yasl.ui.adapter.handler

enum class OrderType {
    Created,
    Alphanumeric,
    Category
}