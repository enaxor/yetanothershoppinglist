package eu.rxsoft.yasl.ui.adapter

import android.app.AlertDialog
import android.content.res.ColorStateList
import android.os.Build
import android.text.InputType
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.setMargins
import androidx.recyclerview.widget.RecyclerView
import eu.rxsoft.yasl.R
import eu.rxsoft.yasl.data.Category
import eu.rxsoft.yasl.data.ListManager
import eu.rxsoft.yasl.ui.adapter.handler.CategoryResourceHandler
import kotlinx.android.synthetic.main.item_edit_item.view.*

class CategoryEditionListAdapter(private var modelArrayList: ArrayList<Category>) : RecyclerView.Adapter<CategoryEditionListAdapter.ItemViewHolder>() {

    fun setList(list : ArrayList<Category>) {
        modelArrayList.clear()
        modelArrayList.addAll(list)
    }

    override fun getItemCount(): Int {
        return modelArrayList.size
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var itemNameTv: TextView = view.edit_item_name
        var itemButtonCategory: ImageButton = view.edit_item_category
        var itemButtonDelete : ImageButton = view.edit_item_delete
        init {
            // fix: disable spellchecker
            itemNameTv.inputType = itemNameTv.inputType or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
        }
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val category : Category = modelArrayList[position]
        holder.itemNameTv.text = category.name
        holder.itemButtonDelete.setOnClickListener {
            val builderX = AlertDialog.Builder(it.context)
            builderX.setTitle("Delete category")
            builderX.setMessage("${category.name} will be deleted")
            builderX.setPositiveButton("Delete" ) { dialog, _ ->
                ListManager.deleteCategory(category.id)
                Runnable {
                    this.setList(ListManager.getCategory(false))
                    this.notifyDataSetChanged()
                }.run()
                dialog.dismiss()
            }
            builderX.setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
            builderX.show()
        }
        holder.itemButtonCategory.background = CategoryResourceHandler.getCategoryDrawable(holder.itemButtonCategory.context, category)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.itemButtonCategory.backgroundTintList = ColorStateList.valueOf(
                CategoryResourceHandler.getColor(holder.itemButtonCategory.context, category.color))
        }
        holder.itemNameTv.setOnLongClickListener {
            // input
            val input = EditText(holder.itemNameTv.context)
            input.inputType = InputType.TYPE_CLASS_TEXT
            input.hint = category.name
            input.setSingleLine()
            // container
            val container = FrameLayout(holder.itemNameTv.context)
            val params = FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            params.leftMargin = holder.itemNameTv.context.resources.getDimensionPixelSize(R.dimen.dialog_margin)
            input.layoutParams = params
            container.addView(input)
            val builderX = AlertDialog.Builder(it.context)
            builderX.setTitle("Change category's name")
            builderX.setView(container)
            builderX.setPositiveButton("Modify category") { dialog, _ ->
                ListManager.updateCategory(category.id, input.text.toString(), null, null)
                Runnable {
                    this.setList(ListManager.getCategory(false))
                    this.notifyDataSetChanged()
                }.run()
                dialog.dismiss()
            }
            builderX.setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
            builderX.show()
            true
        }
        holder.itemButtonCategory.setOnClickListener {
            // change category icon
            val spinnerIcon = Spinner (it.context)
            spinnerIcon.adapter = SpinnerCategoryAdapter(it.context, false, CategoryResourceHandler.iconList)
            val spinnerColor = Spinner (it.context)
            spinnerColor.adapter = SpinnerCategoryAdapter(it.context, true, CategoryResourceHandler.colorList)
            // container
            val container = FrameLayout(it.context)
            val paramsIcon = FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            paramsIcon.setMargins(it.context.resources.getDimensionPixelSize(R.dimen.dialog_margin))
            paramsIcon.gravity = Gravity.START
            val paramsColor = FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            paramsColor.setMargins(it.context.resources.getDimensionPixelSize(R.dimen.dialog_margin))
            paramsColor.gravity = Gravity.END
            spinnerIcon.layoutParams = paramsIcon
            spinnerIcon.setSelection(category.icon)
            spinnerColor.layoutParams = paramsColor
            spinnerColor.setSelection(category.color)
            container.setPadding(90 , 0 ,120 ,0)
            container.addView(spinnerIcon)
            container.addView(spinnerColor)
            // builder
            val builderX = AlertDialog.Builder(it.context)
            builderX.setTitle("Modify category ${category.name}")
            builderX.setView(container)
            builderX.setPositiveButton(R.string.ok) { dialog, _ ->
                ListManager.updateCategory(category.id, null, spinnerIcon.selectedItemPosition, spinnerColor.selectedItemPosition)
                Runnable {
                    this.setList(ListManager.getCategory(false))
                    this.notifyDataSetChanged()
                }.run()
                dialog.dismiss()
            }
            builderX.setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
            builderX.show()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_edit_item, parent, false)
        return ItemViewHolder(view)
    }
}