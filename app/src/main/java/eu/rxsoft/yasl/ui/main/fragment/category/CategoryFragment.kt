package eu.rxsoft.yasl.ui.main.fragment.category

import android.app.AlertDialog
import android.os.Bundle
import android.text.InputType
import android.view.*
import android.widget.EditText
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import eu.rxsoft.yasl.R
import eu.rxsoft.yasl.data.ListManager
import eu.rxsoft.yasl.ui.adapter.CategoryEditionListAdapter
import kotlinx.android.synthetic.main.activity_item_list_edit.*

class CategoryFragment : Fragment() {

    private lateinit var categoryViewModel: CategoryViewModel
    private lateinit var adapter: CategoryEditionListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        categoryViewModel =
            ViewModelProviders.of(this).get(CategoryViewModel::class.java)
        val root = inflater.inflate(R.layout.activity_item_list_edit, container, false)
        // menu
        setHasOptionsMenu(true)
        // observer category list
        categoryViewModel.categoryList.observe(viewLifecycleOwner, Observer {
            adapter = CategoryEditionListAdapter(it)
            item_edition_listView.adapter = adapter
            item_edition_listView.layoutManager = LinearLayoutManager(context)
        })
        return root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_edit, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_add_item -> {
            // input
            val input = EditText(context!!)
            input.inputType = InputType.TYPE_CLASS_TEXT
            input.hint = "ex: fresh food"
            input.setSingleLine()
            // container
            val container = FrameLayout(context!!)
            val params = FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            params.leftMargin = resources.getDimensionPixelSize(R.dimen.dialog_margin)
            input.layoutParams = params
            container.addView(input)
            // builder
            val builderX = AlertDialog.Builder(context!!)
            builderX.setTitle("Create new category")
            builderX.setView(container)
            builderX.setPositiveButton(R.string.ok) { dialog, _ ->
                val inputCategoryName : String = input.text.toString().trim()
                if (inputCategoryName.isNotEmpty()) {
                    ListManager.addCategory(inputCategoryName)
                    Runnable {
                        adapter.setList(ListManager.getCategory(false))
                        adapter.notifyDataSetChanged()
                    }.run()
                }
                dialog.dismiss()
            }
            builderX.setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
            builderX.show()
            true
        }
        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }


}
