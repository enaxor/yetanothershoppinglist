package eu.rxsoft.yasl.ui.adapter

import android.app.AlertDialog
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.setMargins
import androidx.recyclerview.widget.RecyclerView
import eu.rxsoft.yasl.R
import eu.rxsoft.yasl.data.Item
import eu.rxsoft.yasl.data.ListManager
import eu.rxsoft.yasl.ui.adapter.handler.CategoryResourceHandler
import eu.rxsoft.yasl.ui.adapter.handler.OrderType
import kotlinx.android.synthetic.main.item_edit_item.view.*

class ItemEditionListAdapter (private var modelArrayList: ArrayList<Item>) : RecyclerView.Adapter<ItemEditionListAdapter.ItemViewHolder>() {

    fun setList(list : ArrayList<Item>) {
        modelArrayList.clear()
        modelArrayList.addAll(list)
        applyOrdering()
    }

    private var currentOrder = OrderType.Alphanumeric

    private fun applyOrdering() {
        when (currentOrder) {
            OrderType.Created -> modelArrayList.sortByDescending { item -> item.id }
            OrderType.Alphanumeric -> modelArrayList.sortBy { item -> item.name }
            OrderType.Category -> modelArrayList.sortBy { item -> item.category.id }
        }
    }

    fun reorderList() : OrderType {
        // cycle ordering
        currentOrder = when (currentOrder) {
            OrderType.Created -> OrderType.Alphanumeric
            OrderType.Alphanumeric -> OrderType.Category
            OrderType.Category -> OrderType.Created
        }
        // apply ordering
        applyOrdering()
        return currentOrder
    }

    override fun getItemCount(): Int {
        return modelArrayList.size
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var itemNameTv: TextView = view.edit_item_name
        var itemButtonCategory: ImageButton = view.edit_item_category
        var itemButtonDelete : ImageButton = view.edit_item_delete

        init {
            // fix: disable spellchecker
            itemNameTv.inputType = itemNameTv.inputType or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
        }
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item : Item = modelArrayList[position]
        holder.itemNameTv.text = item.name
        holder.itemButtonDelete.setOnClickListener {
            val builderX = AlertDialog.Builder(it.context)
            builderX.setTitle("Delete item")
            builderX.setMessage("${item.name} will be deleted")
            builderX.setPositiveButton("Delete" ) { dialog, _ ->
                ListManager.deleteItem(item.id)
                Runnable {
                    this.setList(ListManager.getItems())
                    this.notifyDataSetChanged()
                }.run()
                dialog.dismiss()
            }
            builderX.setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
            builderX.show()
        }
        holder.itemButtonCategory.background = CategoryResourceHandler.getCategoryDrawable(holder.itemButtonCategory.context, item.category)
        holder.itemNameTv.setOnClickListener { it ->
            // input
            val input = EditText(holder.itemNameTv.context)
            input.inputType = InputType.TYPE_CLASS_TEXT
            input.setText(item.name)
            input.setSingleLine()
            // container
            val container = FrameLayout(holder.itemNameTv.context)
            val params = FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            params.leftMargin = holder.itemNameTv.context.resources.getDimensionPixelSize(R.dimen.dialog_margin)
            input.layoutParams = params
            container.addView(input)
            val builderX = AlertDialog.Builder(it.context)
            builderX.setTitle("Change item's name")
            builderX.setView(container)
            builderX.setPositiveButton("Modify item") { dialog, _ ->
                ListManager.updateItem(item.id, input.text.toString(), null)
                Runnable {
                    this.setList(ListManager.getItems())
                    this.notifyDataSetChanged()
                }.run()
                dialog.dismiss()
            }
            builderX.setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
            builderX.show()
        }
        holder.itemButtonCategory.setOnClickListener { it ->
            // input
            val input = Spinner (it.context)
            val categoryList = ListManager.getCategory()
            val categoryNameList = categoryList.map { it.name }.toTypedArray()
            val adapter: ArrayAdapter<String> = ArrayAdapter(it.context, android.R.layout.simple_spinner_item, categoryNameList)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            input.adapter = adapter
            input.setSelection(categoryNameList.indexOf(item.category.name))
            // container
            val container = FrameLayout(it.context)
            val params = FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(it.context.resources.getDimensionPixelSize(R.dimen.dialog_margin))
            input.layoutParams = params
            container.addView(input)
            // builder
            val builderX = AlertDialog.Builder(it.context)
            builderX.setTitle("Modify category of ${item.name}")
            builderX.setView(container)
            builderX.setPositiveButton(R.string.ok) { dialog, _ ->
                ListManager.updateItem(item.id, null, categoryList[input.selectedItemPosition].id)
                Runnable {
                    this.setList(ListManager.getItems())
                    this.notifyDataSetChanged()
                }.run()
                dialog.dismiss()
            }
            builderX.setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
            builderX.show()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_edit_item, parent, false)
        return ItemViewHolder(view)
    }
}