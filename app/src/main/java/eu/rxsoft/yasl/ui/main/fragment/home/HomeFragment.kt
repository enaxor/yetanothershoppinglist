package eu.rxsoft.yasl.ui.main.fragment.home

import android.app.AlertDialog
import android.os.Bundle
import android.text.InputType
import android.view.*
import android.widget.*
import androidx.core.view.setMargins
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import eu.rxsoft.yasl.R
import eu.rxsoft.yasl.data.Item
import eu.rxsoft.yasl.data.ItemList
import eu.rxsoft.yasl.data.ListManager
import eu.rxsoft.yasl.ui.adapter.ShoppingListAdapter
import eu.rxsoft.yasl.ui.adapter.handler.OrderType
import eu.rxsoft.yasl.ui.main.QuizActivity
import kotlinx.android.synthetic.main.activity_main_shopping_list.*


class HomeFragment : Fragment() {

    private lateinit var currentList: ArrayList<Item>
    private lateinit var adapter: ShoppingListAdapter
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.activity_main_shopping_list, container, false)
        // menu
        setHasOptionsMenu(true)
        // get list from view model
        homeViewModel.shoppingList.observe(viewLifecycleOwner, Observer {
            // init data list
            currentList = it
            // recycler view
            adapter = ShoppingListAdapter(currentList)
            recycler_view.adapter = adapter
            recycler_view.layoutManager = LinearLayoutManager(context)
            // first init of the list
            provideListOrHintMessage()
            // floating action button configuration
            fab.setOnClickListener {
                fabSubMenuToggle()
            }
            // generate new list button configuration
            fab_mini_gen.setOnClickListener {
                if (ListManager.getCurrentList().items.isNotEmpty()) {
                    val builderX = AlertDialog.Builder(context)
                    builderX.setTitle("Replace the current list ?")
                    builderX.setMessage("Creation of a new list will overwrite the current containing ${currentList.size} items. Consider saving current list first.")
                    builderX.setPositiveButton("Overwrite") { _, _ ->
                        val intent = QuizActivity.newIntent(context!!)
                        startActivity(intent)
                    }
                    builderX.setNeutralButton("Merge") {_, _ ->
                        val intent = QuizActivity.newIntent(context!!)
                        intent.putExtra("merge", true)
                        startActivity(intent)
                    }
                    builderX.setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
                    builderX.show()
                } else {
                    val intent = QuizActivity.newIntent(context!!)
                    startActivity(intent)
                }
            }
            // add item to list button configuration
            fab_mini_add.setOnClickListener {
                // autocomplete
                val items = ListManager.getItems()
                val autoCompleteAdapter: ArrayAdapter<String> =
                    ArrayAdapter(context!!, android.R.layout.simple_dropdown_item_1line,
                        items.map { it.name }.toTypedArray()
                    )
                // input
                val input = AutoCompleteTextView(context!!)
                input.inputType = InputType.TYPE_CLASS_TEXT
                input.setAdapter(autoCompleteAdapter)
                input.setSingleLine()
                // container
                val inputLayout = FrameLayout(context!!)
                val params = FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                params.setMargins(context!!.resources.getDimensionPixelSize(R.dimen.dialog_margin))
                input.layoutParams = params
                inputLayout.addView(input)
                val builderX = AlertDialog.Builder(context!!)
                builderX.setTitle("Insert an item")
                builderX.setView(inputLayout)
                builderX.setPositiveButton(R.string.ok) { dialog, _ ->
                    val inputCategoryName: String = input.text.toString().trim()
                    if (inputCategoryName.isNotBlank()) {
                        val newItemId =
                            items.getOrNull(input.listSelection)?.id ?: ListManager.addItem(
                                inputCategoryName
                            )
                        ListManager.addItemToCurrentList(newItemId)
                        reloadCurrentShoppingList()

                    }
                }
                builderX.setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
                builderX.show()
            }
        })
        return root
    }

    override fun onResume() {
        super.onResume()
        fabSubMenuToggle(true)
        reloadCurrentShoppingList()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_main, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    // toolbar action
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_sort_current_list -> {
            Runnable {
                when (adapter.reorderList()) {
                    OrderType.Created -> item.setIcon(R.drawable.openmoji_1f4c5_white)
                    OrderType.Alphanumeric -> item.setIcon(R.drawable.openmoji_1f520_white)
                    OrderType.Category -> item.setIcon(R.drawable.openmoji_1fa9f_white)
                }
                adapter.notifyDataSetChanged()

            }.run()
            true
        }
        R.id.action_clear_current_list -> {
            val builderX = AlertDialog.Builder(context)
            builderX.setTitle("Clear the current list ?")
            builderX.setPositiveButton("Clear") { dialog, _ ->
                ListManager.clearCurrentList()
                reloadCurrentShoppingList()
                dialog.dismiss()
            }
            builderX.setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
            builderX.show()
            true
        }
        R.id.action_save_list -> {
            val currentList: ItemList? = ListManager.getCurrentList()
            if (currentList != null && currentList.items.isNotEmpty()) {
                // input
                val input = EditText(context)
                input.inputType = InputType.TYPE_CLASS_TEXT
                input.hint = "ex: vegetables"
                input.setSingleLine()
                // container
                val container = FrameLayout(context!!)
                val params = FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                params.leftMargin = resources.getDimensionPixelSize(R.dimen.dialog_margin)
                input.layoutParams = params
                container.addView(input)
                // builder
                val builderX = AlertDialog.Builder(context)
                builderX.setTitle("Set a name for this list")
                builderX.setView(container)
                builderX.setPositiveButton(R.string.ok) { dialog, _ ->
                    currentList.name = input.text.toString()
                    ListManager.archive(currentList)
                    dialog.dismiss()
                }
                builderX.setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
                builderX.show()
            } else {
                Toast.makeText(context, "Your grocery list is empty !", Toast.LENGTH_SHORT).show()
            }
            true
        }
        R.id.action_load_list -> {
            val archived: ArrayList<ItemList> = ListManager.getListArchived()
            if (archived.isEmpty()) {
                Toast.makeText(context, "No saved list found !", Toast.LENGTH_SHORT).show()
            } else {
                val archivesNameList: Array<String> = archived.map { it.name }.toTypedArray()
                // builderArchiveDialog
                val builderArchiveDialog = AlertDialog.Builder(context)
                builderArchiveDialog.setTitle("Saved Shopping lists")
                builderArchiveDialog.setItems(
                    archivesNameList
                ) { dialog, which ->
                    ListManager.loadArchived(archived[which].id)
                    reloadCurrentShoppingList()
                    dialog.dismiss()
                }
                // archiveDialog
                val archiveDialog = builderArchiveDialog.create()
                archiveDialog.setOnShowListener {
                    archiveDialog.listView.onItemLongClickListener =
                        AdapterView.OnItemLongClickListener { _, _, position, _ ->
                            val builderDeleteDialog = AlertDialog.Builder(context)
                            builderDeleteDialog.setTitle("Delete saved list")
                            builderDeleteDialog.setMessage("Are you sure? Can't be undone")
                            builderDeleteDialog.setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
                            builderDeleteDialog.setPositiveButton(R.string.ok) { _, _ ->
                                ListManager.deleteArchivedList(archived[position].id)
                                archiveDialog.dismiss()
                            }
                            builderDeleteDialog.show()
                            true
                        }
                }
                archiveDialog.show()
            }
            true
        }

        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

    //boolean flag to know if main FAB is in open or closed state.
    private var fabExpanded = false

    private fun fabSubMenuToggle(forceClose: Boolean = false) {
        if (fabExpanded || forceClose) {
            fab_mini_add.visibility = View.INVISIBLE
            fab_mini_gen.visibility = View.INVISIBLE
            fab.setImageResource(R.drawable.openmoji_e25f_white)
            fabExpanded = false
        } else if (!fabExpanded) {
            fab_mini_add.visibility = View.VISIBLE
            fab_mini_gen.visibility = View.VISIBLE
            fab.setImageResource(android.R.drawable.ic_menu_close_clear_cancel)
            fabExpanded = true
        }
    }

    private fun provideListOrHintMessage() {
        if (currentList.isEmpty()) {
            recycler_view.visibility = View.GONE
            no_list_text_content.visibility = View.VISIBLE
        } else {
            no_list_text_content.visibility = View.GONE
            recycler_view.visibility = View.VISIBLE
        }
    }

    private fun reloadCurrentShoppingList() {
        val currentListItem: ItemList = ListManager.getCurrentList()
        currentList = currentListItem.items
        Runnable {
            adapter.setList(currentList)
            adapter.notifyDataSetChanged()
        }.run()
        // show list, or show empty list text
        provideListOrHintMessage()
    }

}
