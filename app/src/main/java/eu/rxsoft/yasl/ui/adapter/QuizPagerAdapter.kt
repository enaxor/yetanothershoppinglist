package eu.rxsoft.yasl.ui.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.viewpager.widget.PagerAdapter
import eu.rxsoft.yasl.R
import eu.rxsoft.yasl.data.Item
import eu.rxsoft.yasl.ui.adapter.handler.CategoryResourceHandler
import kotlinx.android.synthetic.main.quizz_view_pager.view.*


class QuizPagerAdapter(private val mContext: Context, private val items: ArrayList<Item>) :
    PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return items.size
    }

    override fun getItemPosition(`object`: Any): Int {
        // case of an item deletion
        if (`object` is CoordinatorLayout) {
            return POSITION_NONE
        }
        // normal case of item browsing
        val item: Item = `object` as Item
        val indexOf = items.indexOf(item)
        return if (indexOf >= 0) {
            indexOf
        } else {
            POSITION_NONE
        }
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        //  val modelObject: View =  views[position]
        val inflater = LayoutInflater.from(mContext)
        val layout =
            inflater.inflate(R.layout.quizz_view_pager, container, false) as ViewGroup
        // fill content
        val item = items[position]
        val colorStateList = ColorStateList.valueOf(
            CategoryResourceHandler.getColor(
                container.context,
                item.category.color
            )
        )
        layout.fullscreen_content_item.text = item.name
        layout.fullscreen_text_category.text = item.category.name
        layout.fullscreen_text_category.setTextColor(colorStateList)
        layout.fullscreen_icon_category.background =
            CategoryResourceHandler.getCategoryDrawable(container.context, item.category)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            layout.fullscreen_icon_category.backgroundTintList = colorStateList
        }
        container.addView(layout)
        return layout
    }

    fun removeItem(position: Int) {
        items.removeAt(position)
        notifyDataSetChanged()
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        /* clattering shutters */
    }
}