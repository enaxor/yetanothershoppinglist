package eu.rxsoft.yasl.ui.adapter

import android.app.AlertDialog
import android.os.Handler
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageButton
import androidx.recyclerview.widget.RecyclerView
import eu.rxsoft.yasl.R
import eu.rxsoft.yasl.data.Item
import eu.rxsoft.yasl.data.ListManager
import eu.rxsoft.yasl.ui.adapter.handler.CategoryResourceHandler
import eu.rxsoft.yasl.ui.adapter.handler.OrderType
import kotlinx.android.synthetic.main.shopping_list_item.view.*

class ShoppingListAdapter(currentList: ArrayList<Item>) : RecyclerView.Adapter<ShoppingListAdapter.ItemViewHolder>() {

    private var modelArrayList: ArrayList<Item> = currentList
    private var currentOrder = OrderType.Category

    override fun getItemCount(): Int {
        return modelArrayList.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = modelArrayList[position]
        // checkbox text
        holder.checkBox.text = item.name
        holder.checkBox.isChecked = item.checked
        holder.checkBox.setOnClickListener {
            item.checked = holder.checkBox.isChecked
            ListManager.checkItem(item)
            modelArrayList.sortBy { item -> item.checked }
            // wait a bit before update list so the user see it checked before resorting
            Handler().postDelayed({
                Runnable {
                    this.notifyDataSetChanged()
                }.run()
            }, 200)

        }
        holder.checkBox.setOnLongClickListener {
            val builderX = AlertDialog.Builder(holder.checkBox.context)
            builderX.setTitle("Remove item")
            builderX.setMessage("${item.name} will be deleted.")
            builderX.setPositiveButton("Delete") { _, _ ->
                for (itm  in modelArrayList) {
                    if (itm.id == item.id) {
                        modelArrayList.remove(itm)
                        ListManager.deleteItemFromCurrentList(item.id)
                        // update list
                        Runnable {
                            this.notifyDataSetChanged()
                        }.run()
                        break
                    }
                }
            }
            builderX.setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
            builderX.show()
            true
        }
        // checkbox icon
        holder.icon.background = CategoryResourceHandler.getCategoryDrawable(holder.icon.context, item.category)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.shopping_list_item, parent, false)
        return ItemViewHolder(view)
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val checkBox: CheckBox = view.item_checkbox
        val icon: ImageButton = view.item_category

        init {
            // fix: disable spellchecker
            checkBox.inputType = checkBox.inputType or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
        }

    }

    fun setList(list : ArrayList<Item>) {
        list.sortBy { item -> item.checked }
        modelArrayList.clear()
        modelArrayList.addAll(list)
    }

    fun reorderList(): OrderType {
        // cycle ordering
        currentOrder = when (currentOrder) {
            OrderType.Created -> OrderType.Alphanumeric
            OrderType.Alphanumeric -> OrderType.Category
            OrderType.Category -> OrderType.Created
        }
        // apply ordering
        applyOrdering()
        return currentOrder
    }

    private fun applyOrdering() {
        when (currentOrder) {
            OrderType.Created -> modelArrayList.sortByDescending { item -> item.id }
            OrderType.Alphanumeric -> modelArrayList.sortBy { item -> item.name }
            OrderType.Category -> modelArrayList.sortBy { item -> item.category.id }
        }
        // sort by checked anyway
        modelArrayList.sortBy { item -> item.checked }
    }

}