package eu.rxsoft.yasl.ui.main.fragment.backup

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import eu.rxsoft.yasl.R
import eu.rxsoft.yasl.data.Category
import eu.rxsoft.yasl.data.Item
import eu.rxsoft.yasl.data.ListManager
import eu.rxsoft.yasl.ui.adapter.CategoryEditionListAdapter
import kotlinx.android.synthetic.main.fragment_backup.*
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.Logger
import kotlin.collections.ArrayList


class BackupFragment : Fragment() {

    private lateinit var backupViewModel: BackupViewModel
    private lateinit var adapter: CategoryEditionListAdapter

    @SuppressLint("SimpleDateFormat")
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        backupViewModel =
                ViewModelProviders.of(this).get(BackupViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_backup, container, false)
        // menu
        setHasOptionsMenu(true)
        // buttons event
        backupViewModel.text.observe(viewLifecycleOwner, Observer {
            backup_import.setOnClickListener {
                // get existing category to merge
                val dbCategory = ListManager.getCategory(true)
                val newCategory = ArrayList<Category>()
                val newItems = ArrayList<Item>()
                // create file
                val pattern = "yyyy-MM-dd"
                val simpleDateFormat = SimpleDateFormat(pattern)
                val date: String = simpleDateFormat.format(Date())
                val file = File(this.context?.applicationInfo?.dataDir, "backup_$date.csv")
                val contents = file.readLines()
                // parse csv
                for (line in contents) {
                    val dataLine = line.split(",")
                    if (dataLine[0] == "c") {
                        // new category
                        newCategory.add(Category(id = dataLine[1].toLong(), name = dataLine[2], color = 0, icon = 0))
                    } else if (dataLine[0] == "i") {
                        // new item
                        val id = dataLine[1].toLong()
                        val name = dataLine[2]
                        for (c in newCategory) {
                            if (c.id == dataLine[3].toLong()) {
                                newItems.add(Item(id, name, false, c))
                            }
                        }
                    }
                }
                // fill db with data
                for (nc in newCategory) {
                    for (dbc in dbCategory) {
                        if (dbc.name == nc.name) {
                            // merge category id for items
                            for (ni in newItems) {
                                if (ni.category.name == dbc.name && ni.category.id != dbc.id) {
                                    ni.category = dbc
                                }
                            }
                            break
                        }
                    }
                    // add missing category
                    ListManager.addCategory(nc.name)
                }
                // add missing item
                for (ni in newItems) {
                    ListManager.addItem(ni.name)
                }
                Snackbar.make(
                        root,
                        "Done importing",
                        Snackbar.LENGTH_SHORT
                ).setAction(R.string.dismiss) {}.show()
            }
            backup_export.setOnClickListener {

                val sb = StringBuilder()
                // fill category
                for (category in ListManager.getCategory(false)) {
                    sb.append("c,${category.id},${category.name}\n")
                }
                // fill items
                for (item in ListManager.getItems(false)) {
                    sb.append("i,${item.id},${item.name},${item.category.id}\n")
                }
                // create file
                val pattern = "yyyy-MM-dd"
                val simpleDateFormat = SimpleDateFormat(pattern)
                val date: String = simpleDateFormat.format(Date())
                // Environment.getDataDirectory() return /data/x
                val file = File(this.context?.applicationInfo?.dataDir, "backup_$date.csv")
                // write data
                file.writeText(sb.toString())
                // log
                Snackbar.make(
                        root,
                        "Path: ${file.absolutePath}",
                        Snackbar.LENGTH_INDEFINITE
                ).setAction(R.string.dismiss) {}.show()

            }
        })
        return root
    }
//    companion object {
//        val LOG = Logger.getLogger("BackupFragment")
//    }
}
